﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento
{
    public class CalculosFolhaPagamento
    {
        public decimal Inss(decimal salario, decimal aliquota)
        {
            decimal inss = salario * (aliquota / 100);
            return inss;
        }

        public decimal IR(decimal salario, decimal aliquota)
        {
            decimal IR = salario * (aliquota / 100);
            return IR;
        }




    }
}
