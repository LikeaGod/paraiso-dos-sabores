﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Imposto_de_Renda
{
    class ImpostoRendaDTO
    {
        public int Id{ get; set; }
        public decimal Faixa_Salario{ get; set; }
        public decimal Aliquota{ get; set; }

    }
}
