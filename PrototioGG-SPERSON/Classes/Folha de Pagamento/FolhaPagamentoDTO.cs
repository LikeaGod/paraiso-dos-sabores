﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento
{
    class FolhaPagamentoDTO
    {

        public int Id { get; set; }
        public DateTime Data_Folha_Pagamento { get; set; }
        public decimal Valor_Hora { get; set; }
        public decimal Valor_Bruto { get; set; }
        public decimal Valor_Liquido { get; set; }
        public int Fk_Ponto { get; set; }
        public int Fk_Descontos { get; set; }
        public int Fk_INSS { get; set; }
        public int Fk_Imposto_Renda { get; set; }
    }
}
