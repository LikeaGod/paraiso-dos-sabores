﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento
{
    class DatabaseFolhaPagamento
    {
        public void Alterar(FolhaPagamentoDTO folha)
        {
            string script = @"UPDATE tb_folha_pagamento SET dt_Folha_Pagamento = @dt_Folha_Pagamento, vl_hora = @vl_hora, vl_bruto = @vl_bruto, vl_liquido = @vl_liquido, fk_ponto = @fk_ponto, fk_imposto_renda = @fk_imposto_renda, fk_inss = @fk_inss, fk_descontos = @fk_descontos WHERE id_folha_pagamento = @id_folha_pagamento";
            List<MySqlParameter> parms = new List<MySqlParameter>
            {
                new MySqlParameter("id_folha_pagamento", folha.Id),
                new MySqlParameter("dt_Folha_Pagamento", folha.Data_Folha_Pagamento),
                new MySqlParameter("vl_hora", folha.Valor_Hora),
                new MySqlParameter("vl_bruto", folha.Valor_Bruto),
                new MySqlParameter("vl_liquido", folha.Valor_Liquido),
                new MySqlParameter("fk_ponto", folha.Fk_Ponto),
                new MySqlParameter("fk_imposto_renda", folha.Fk_Imposto_Renda),
                new MySqlParameter("fk_inss", folha.Fk_INSS),
                new MySqlParameter("fk_descontos", folha.Fk_Descontos)
            };
            Database db = new Database(); db.ExecuteInsertScript(script, parms);
        }

    }
}
