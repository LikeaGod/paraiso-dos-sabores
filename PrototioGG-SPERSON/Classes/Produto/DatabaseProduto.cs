﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Produto
{
    class DatabaseProduto      
    {
        Database db = new Database();

        public int Salvar ( DTOProduto dto)
        {
            string script = @"INSERT INTO tb_produto (nm_produto, ds_produto, ds_categoria, vl_unitario_compra, vl_unitario_venda, ds_marca)
                                VALUES (@nm_produto, @ds_produto, @ds_categoria, @vl_unitario_compra, @vl_unitario_venda, @ds_marca)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("ds_produto", dto.Descricao));
            parms.Add(new MySqlParameter("ds_categoria", dto.Categoria));
            parms.Add(new MySqlParameter("vl_unitario_compra", dto.Valor_Unit_Compra));
            parms.Add(new MySqlParameter("vl_unitario_venda", dto.Valor_Unit_Venda));
            parms.Add(new MySqlParameter("ds_marca", dto.Marca));

            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;

        }

        public List<DTOProduto> Listar(string nome)
        {
            string script = @" SELECT * FROM tb_produto 
                                WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + nome + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTOProduto> lista = new List<DTOProduto>();

            while(reader.Read())
            {
                DTOProduto dto = new DTOProduto();
                dto.ID = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Descricao = reader.GetString("ds_produto");
                dto.Categoria = reader.GetString("ds_categoria");
                dto.Valor_Unit_Compra = reader.GetDecimal("vl_unitario_compra");
                dto.Valor_Unit_Venda = reader.GetDecimal("vl_unitario_venda");
                dto.Marca = reader.GetString("ds_marca");

                lista.Add(dto);
            }

            reader.Close();
            return lista;

        }

            



        }
}
