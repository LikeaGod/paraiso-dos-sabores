﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Produto
{
    class DTOProduto
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Categoria { get; set; }
        public decimal Valor_Unit_Venda { get; set; }
        public decimal Valor_Unit_Compra { get; set; }
        public string Marca { get; set; }
    }
}
