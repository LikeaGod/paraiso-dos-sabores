﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Produto
{
    class BusinessProduto
    {
        DatabaseProduto db = new DatabaseProduto();

        public int Salvar(DTOProduto dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Informe o Nome do produto");
            }

            if (dto.Marca == string.Empty)
            {
                throw new ArgumentException("Informe a marca do produto");

            }

            return db.Salvar(dto);
        }

        public List<DTOProduto> Lista(string nome)
        {
            if (nome == "")
            {
                nome = string.Empty;

            }

            return db.Listar(nome);

        }



    }
}
