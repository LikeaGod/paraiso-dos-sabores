﻿
using PrototioGG_SPERSON.DB.Departamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Funcionario
{
    class BusinessFuncionario
    {
        DatabaseDepartamento dbb = new DatabaseDepartamento();
        DatabaseFuncionario db = new DatabaseFuncionario();
        public int Salvar(DTOFuncionario dto, DTODepartamento dto2)
        {
            List<DTODepartamento> dtooos = new List<DTODepartamento>();

            dtooos = dbb.Listar(dto2.Nome);
            foreach (DTODepartamento item in dtooos)
            {
                dto2.ID = item.ID;
            }
            dto.FK_Departamento = dto2.ID;

            int id = db.Salvar(dto);
            return id;
        }

        public DTOFuncionario Logou(DTOFuncionario dto)
        {
            dto = db.Logar(dto);

            return dto;
        }

        public List<DTOFuncionario> Consultar(String funcionario)
        {
            if (funcionario == "")
            {
                funcionario = string.Empty;

            }
            List<DTOFuncionario> dto = new List<DTOFuncionario>();

            dto = db.Consultar(funcionario);
            
            return dto;

        }
    }
}
