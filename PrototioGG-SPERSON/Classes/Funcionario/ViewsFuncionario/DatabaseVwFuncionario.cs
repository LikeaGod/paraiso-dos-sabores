﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Funcionario.ViewsFuncionario
{
    class DatabaseVwFuncionario
    {
        public List<DTOVwFuncionario> ConsultarView(DTOVwFuncionario dto)
        {
            string script = @"SELECT * FROM   ggs_person.vw_funcionario_departamento
                                       WHERE  nm_funcionario   
                                        LIKE @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + dto.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTOVwFuncionario> dto3 = new List<DTOVwFuncionario>();
            while (reader.Read())
            {
                DTOVwFuncionario dto2 = new DTOVwFuncionario();
                dto2.Nome = reader.GetString("nm_funcionario");
                dto2.RG = reader.GetString("ds_rg");
                dto2.Rua = reader.GetString("ds_rua");
                dto2.Telefone = reader.GetString("num_tel");
                dto2.Celular = reader.GetString("num_cel");
                dto2.Bairro = reader.GetString("ds_bairro");
                dto2.Email = reader.GetString("ds_email");
                dto2.CEP = reader.GetString("ds_cep");
                dto2.Cidade = reader.GetString("ds_cidade");
                dto2.Departamento = reader.GetString("nm_departamento");
                dto2.Estado = reader.GetString("ds_estado");
                dto2.Nascimento = reader.GetDateTime("dt_nascimento");
                dto2.ValorSalario = reader.GetDecimal("vl_salario");

                dto3.Add(dto2);
            }

            return dto3;
        }
    }
}
