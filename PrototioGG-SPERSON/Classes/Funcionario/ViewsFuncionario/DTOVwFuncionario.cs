﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Funcionario.ViewsFuncionario
{
    class DTOVwFuncionario
    {
        public string Nome { get; set; }

        public string RG { get; set; }

        public string Telefone { get; set; }

        public string Celular { get; set; }

        public string Email { get; set; }

        public decimal ValorSalario { get; set; }

        public string Departamento { get; set; }

        public DateTime Nascimento { get; set; }

        public string CEP { get; set; }

        public string Rua { get; set; }

        public string Cidade { get; set; }

        public string Bairro { get; set; }

        public string Estado { get; set; }

    }
}
