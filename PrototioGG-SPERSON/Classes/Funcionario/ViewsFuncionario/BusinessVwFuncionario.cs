﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Funcionario.ViewsFuncionario
{
    class BusinessVwFuncionario
    {
        public List<DTOVwFuncionario> ConsultarView(DTOVwFuncionario dto)
        {
            DatabaseVwFuncionario db = new DatabaseVwFuncionario();
            return db.ConsultarView(dto); 
        }
    }
}
