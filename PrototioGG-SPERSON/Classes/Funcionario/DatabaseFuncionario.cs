﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB.Departamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Funcionario
{
    class DatabaseFuncionario
    {
        Database db = new Database();

        public int Salvar(DTOFuncionario dto)
        {
            string script = @"INSERT INTO tb_funcionario(nm_funcionario, ds_cpf, ds_rg, num_tel, num_cel, ds_rua, ds_bairro, ds_cidade, ds_estado, ds_cep, ds_username, ds_password, ds_email, dt_nascimento, fk_departamento) VALUES (@nm_funcionario, @ds_cpf, @ds_rg, @num_tel, @num_cel, @ds_rua, @ds_bairro, @ds_cidade, @ds_estado, @ds_cep,  @ds_username, @ds_password, @ds_email, @dt_nascimento, @fk_departamento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Funcionario));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("num_tel", dto.Telefone));
            parms.Add(new MySqlParameter("num_cel", dto.Celular));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_username", dto.UserName));
            parms.Add(new MySqlParameter("ds_password", dto.Password));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("fk_departamento", dto.FK_Departamento));

            int id = db.ExecuteInsertScriptWithPk(script, parms);

            return id;
        }

        public DTOFuncionario Logar(DTOFuncionario dto)
        {
            string script = @"select * from tb_funcionario where ds_username = @ds_username and ds_password = @ds_password)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_username", dto.UserName));
            parms.Add(new MySqlParameter("ds_password", dto.Password));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            dto = null;
            if (reader.Read())
            {
                dto.ID = reader.GetInt32("id_funcionario");
                dto.Funcionario = reader.GetString("nm_funcionario");
                dto.CPF = reader.GetString("ds_cpf");
                dto.RG = reader.GetString("ds_rg");
                dto.Telefone = reader.GetString("num_tel");
                dto.Celular = reader.GetString("num_cel");
                dto.Rua = reader.GetString("ds_rua");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Estado = reader.GetString("ds_estado");
                dto.CEP = reader.GetString("ds_cep");
                dto.UserName = reader.GetString("ds_username");
                dto.Password = reader.GetString("ds_password");
                dto.Email = reader.GetString("ds_email");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.FK_Departamento = reader.GetInt32("fk_departamento");
               
            }
            return dto;

        }

        public List<DTOFuncionario> Consultar (String funcionario)
        {
            string script = @" SELECT * FROM tb_funcionario 
                                WHERE nm_funcionario like @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + funcionario + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTOFuncionario> lista = new List<DTOFuncionario>();

            while (reader.Read())
            {
                DTOFuncionario dto = new DTOFuncionario();
                dto.ID = reader.GetInt32("id_funcionario");
                dto.Funcionario = reader.GetString("nm_funcionario");
                dto.CPF = reader.GetString("ds_cpf");
                dto.RG = reader.GetString("ds_rg");
                dto.Telefone = reader.GetString("num_tel");
                dto.Celular = reader.GetString("num_cel");
                dto.Rua = reader.GetString("ds_rua");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Estado = reader.GetString("ds_estado");
                dto.CEP = reader.GetString("ds_cep");
                dto.Salario = reader.GetDecimal("vl_salario");
                dto.UserName = reader.GetString("ds_username");
                dto.Password = reader.GetString("ds_password");
                dto.Email = reader.GetString("ds_email");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.FK_Departamento = reader.GetInt32("fk_departamento");

                lista.Add(dto);
            }

            reader.Close();
            return lista;

        }
    }
}
