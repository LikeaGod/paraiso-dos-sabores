﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Fornecedor
{
    class BusinessFornecedor
    {
        DatabaseFornecedor db = new DatabaseFornecedor();

        public int Salvar(DTOFornecedor dto)
        {
            if (dto.Fornecedor == string.Empty)
            {
                throw new ArgumentException("Informe o Nome do fornecedor");
            }

            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("informações de contato obrigatorias, por favor informe o email do fornecedor");

            }

            if (dto.Estado == string.Empty)
            {
                throw new ArgumentException("informações de contato obrigatorias, por favor informe o estado do fornecedor");

            }

            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException(" por favor informe o CEP do fornecedor");

            }

            if (dto.CNPJ == string.Empty)
            {
                throw new ArgumentException("informe o CNPJ do fornecedor");

            }

            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("informações de contato obrigatorias, por favor informe o telefone do fornecedor");

            }


            return db.Salvar(dto);
        }



        public List<DTOFornecedor> Listar (string nome)
        {
            if (nome == "")
            {
                nome = string.Empty;

            }

            return db.Listar(nome);

        }

    }
}
