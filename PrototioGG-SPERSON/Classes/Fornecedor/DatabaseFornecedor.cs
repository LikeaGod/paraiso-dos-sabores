﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Fornecedor
{
    class DatabaseFornecedor
    {
        Database db = new Database();

        public int Salvar(DTOFornecedor dto)
        {
            string script = @"INSERT INTO tb_fornecedor (nm_fornecedor, ds_cnpj, ds_rua, ds_bairro, ds_cidade, ds_estado, ds_cep, ds_email, num_tel )
                                VALUES (@nm_fornecedor, @ds_cnpj, @ds_rua, @ds_bairro, @ds_cidade, @ds_estado, @ds_cep, @ds_email, @num_tel )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Fornecedor));
            parms.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("num_tel", dto.Telefone));

            int id = db.ExecuteInsertScriptWithPk(script, parms);
            return id;

        }

        public List<DTOFornecedor> Listar(string fornecedor)
        {
            string script = @" SELECT * FROM tb_fornecedor 
                                WHERE nm_fornecedor like @nm_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", "%" + fornecedor + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTOFornecedor> lista = new List<DTOFornecedor>();

            while (reader.Read())
            {
                DTOFornecedor dto = new DTOFornecedor();
                dto.ID = reader.GetInt32("id_fornecedor");
                dto.Fornecedor = reader.GetString("nm_fornecedor");
                dto.CNPJ = reader.GetString("ds_cnpj");
                dto.Rua = reader.GetString("ds_rua");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Estado = reader.GetString("ds_estado");
                dto.CEP = reader.GetString("ds_cep");
                dto.Email = reader.GetString("ds_email");
                dto.Telefone = reader.GetString("num_tel");
                
                lista.Add(dto);

            }
            reader.Close();

            return lista;


        }


    }
}
