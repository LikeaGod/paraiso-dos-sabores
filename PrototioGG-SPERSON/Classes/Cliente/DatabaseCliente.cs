﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Cliente
{
    class DatabaseCliente
    {
        Database db = new Database();

        public int Salvar(DTOcliente dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_cliente, ds_cpf, num_tel, num_cel, ds_email, ds_rua, ds_bairro, ds_cidade, ds_estado, ds_cep )
                                VALUES (@nm_cliente, @ds_cpf, @num_tel, @num_cel, @ds_email, @ds_rua, @ds_bairro, @ds_cidade, @ds_estado, @ds_cep)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("num_tel", dto.Telefone));
            parms.Add(new MySqlParameter("num_cel", dto.Celular));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));


            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;

        }

        public List<DTOcliente> Listar(string nome)
        {
            string script = @" SELECT * FROM tb_cliente
                                WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + nome + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTOcliente> lista = new List<DTOcliente>();

            while (reader.Read())
            {
                DTOcliente dto = new DTOcliente();
                dto.ID = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.CPF = reader.GetString("ds_cpf");
                dto.Telefone = reader.GetString("num_tel");
                dto.Celular = reader.GetString("num_cel");
                dto.Email = reader.GetString("ds_email");
                dto.Rua = reader.GetString("ds_rua");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Estado = reader.GetString("ds_estado");
                dto.CEP = reader.GetString("ds_cep");

                lista.Add(dto);
            }

            reader.Close();
            return lista;

        }

    }
}
