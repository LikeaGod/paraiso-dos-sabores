﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Cliente
{
    class BusinessCliente
    {
        DatabaseCliente db = new DatabaseCliente();

        public int Salvar(DTOcliente dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Informe o Nome do cliente");
            }

            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("informações de contato obrigatorias, por favor informe o Numero de Celular do cliente");

            }

            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("informações de contato obrigatorias, por favor informe numero de telefone do cliente");

            }

            return db.Salvar(dto);
        }



        public List<DTOcliente> Lista(string nome)
        {
            if (nome == "")
            {
                nome = string.Empty;

            }

            return db.Listar(nome);

        }
    }
}
