﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Departamento
{
    class DatabaseDepartamento
    {
        Database db = new Database();

        public int Salvar (DTODepartamento dto)
        {
            string script = @"INSERT INTO tb_departamento (nm_departamento, vl_salario)
                                VALUES ( @nm_departamento , @vl_salario)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_departamento", dto.Nome));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));

            
            int id = db.ExecuteInsertScriptWithPk(script, parms);
            return id;

        }

        public List<DTODepartamento> Listar(string departamento)
        {
            string script = @" SELECT * FROM tb_departamento 
                                WHERE nm_departamento like @nm_departamento"; 
                                                        
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_departamento", "%" + departamento));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTODepartamento> lista = new List<DTODepartamento>();

            while (reader.Read())
            {
                DTODepartamento dto = new DTODepartamento();
                dto.ID = reader.GetInt32("id_departamento");
                dto.Nome = reader.GetString("nm_departamento");
                dto.Salario = reader.GetDecimal("vl_salario");

                lista.Add(dto);

            }
            reader.Close();

            return lista;


        }
        public List<DTODepartamento> Listar2()
        {
            string script = @" SELECT * FROM tb_departamento";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<DTODepartamento> lista = new List<DTODepartamento>();

            while (reader.Read())
            {
                DTODepartamento dto = new DTODepartamento();
                dto.ID = reader.GetInt32("id_departamento");
                dto.Nome = reader.GetString("nm_departamento");
                dto.Salario = reader.GetDecimal("vl_salario");

                lista.Add(dto);

            }
            reader.Close();

            return lista;


        }



    }
}
