﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Fornecedor;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmFornecedorConsultar : UserControl
    {
        public FrmFornecedorConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarFornecedor_Click(object sender, EventArgs e)
        {
            string fornecedor = txtBuscarFornecedor.Text;

            BusinessFornecedor bus = new BusinessFornecedor();
            List<DTOFornecedor> listar = bus.Listar(fornecedor);

            dgvFornecedor.AutoGenerateColumns = false;
            dgvFornecedor.DataSource = listar;
            

            
        }

        private void panelConsultarFORNECEDORES_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
