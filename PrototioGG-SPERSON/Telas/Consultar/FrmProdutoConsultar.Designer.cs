﻿namespace PrototioGG_SPERSON.Telas.Consultar
{
    partial class FrmProdutoConsultar
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelConsultarPRODUTOS = new System.Windows.Forms.Panel();
            this.btnBuscarProdutos = new System.Windows.Forms.Button();
            this.txtBuscarProduto = new System.Windows.Forms.TextBox();
            this.dgvProdutos = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Marca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Categoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.panelConsultarPRODUTOS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdutos)).BeginInit();
            this.SuspendLayout();
            // 
            // panelConsultarPRODUTOS
            // 
            this.panelConsultarPRODUTOS.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelConsultarPRODUTOS.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources._21;
            this.panelConsultarPRODUTOS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelConsultarPRODUTOS.Controls.Add(this.btnBuscarProdutos);
            this.panelConsultarPRODUTOS.Controls.Add(this.txtBuscarProduto);
            this.panelConsultarPRODUTOS.Controls.Add(this.dgvProdutos);
            this.panelConsultarPRODUTOS.Controls.Add(this.label1);
            this.panelConsultarPRODUTOS.Location = new System.Drawing.Point(0, 0);
            this.panelConsultarPRODUTOS.Name = "panelConsultarPRODUTOS";
            this.panelConsultarPRODUTOS.Size = new System.Drawing.Size(984, 492);
            this.panelConsultarPRODUTOS.TabIndex = 0;
            this.panelConsultarPRODUTOS.Paint += new System.Windows.Forms.PaintEventHandler(this.panelConsultarPRODUTOS_Paint);
            // 
            // btnBuscarProdutos
            // 
            this.btnBuscarProdutos.BackColor = System.Drawing.Color.White;
            this.btnBuscarProdutos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarProdutos.ForeColor = System.Drawing.Color.Black;
            this.btnBuscarProdutos.Location = new System.Drawing.Point(176, 79);
            this.btnBuscarProdutos.Name = "btnBuscarProdutos";
            this.btnBuscarProdutos.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarProdutos.TabIndex = 3;
            this.btnBuscarProdutos.Text = "Buscar";
            this.btnBuscarProdutos.UseVisualStyleBackColor = false;
            this.btnBuscarProdutos.Click += new System.EventHandler(this.btnBuscarProdutos_Click);
            // 
            // txtBuscarProduto
            // 
            this.txtBuscarProduto.Location = new System.Drawing.Point(14, 79);
            this.txtBuscarProduto.Name = "txtBuscarProduto";
            this.txtBuscarProduto.Size = new System.Drawing.Size(143, 20);
            this.txtBuscarProduto.TabIndex = 2;
            this.txtBuscarProduto.TextChanged += new System.EventHandler(this.txtBuscarProduto_TextChanged);
            // 
            // dgvProdutos
            // 
            this.dgvProdutos.AllowUserToAddRows = false;
            this.dgvProdutos.BackgroundColor = System.Drawing.Color.White;
            this.dgvProdutos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Nome,
            this.Valor,
            this.Column1,
            this.Marca,
            this.Categoria,
            this.Descricao});
            this.dgvProdutos.GridColor = System.Drawing.SystemColors.HotTrack;
            this.dgvProdutos.Location = new System.Drawing.Point(15, 137);
            this.dgvProdutos.Name = "dgvProdutos";
            this.dgvProdutos.ReadOnly = true;
            this.dgvProdutos.RowHeadersVisible = false;
            this.dgvProdutos.Size = new System.Drawing.Size(949, 333);
            this.dgvProdutos.TabIndex = 1;
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "Nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // Valor
            // 
            this.Valor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Valor.DataPropertyName = "Valor_Unit_Venda";
            this.Valor.HeaderText = "Valor de Venda";
            this.Valor.Name = "Valor";
            this.Valor.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Valor_Unit_Compra";
            this.Column1.HeaderText = "Valor de Compra";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Marca
            // 
            this.Marca.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Marca.DataPropertyName = "Marca";
            this.Marca.HeaderText = "Marca";
            this.Marca.Name = "Marca";
            this.Marca.ReadOnly = true;
            // 
            // Categoria
            // 
            this.Categoria.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Categoria.DataPropertyName = "Categoria";
            this.Categoria.HeaderText = "Categoria";
            this.Categoria.Name = "Categoria";
            this.Categoria.ReadOnly = true;
            // 
            // Descricao
            // 
            this.Descricao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Descricao.DataPropertyName = "Descricao";
            this.Descricao.HeaderText = "Descrição";
            this.Descricao.Name = "Descricao";
            this.Descricao.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Book Antiqua", 27.75F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(7, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 45);
            this.label1.TabIndex = 0;
            this.label1.Text = "Produtos";
            // 
            // FrmProdutoConsultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelConsultarPRODUTOS);
            this.Name = "FrmProdutoConsultar";
            this.Size = new System.Drawing.Size(984, 492);
            this.panelConsultarPRODUTOS.ResumeLayout(false);
            this.panelConsultarPRODUTOS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdutos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelConsultarPRODUTOS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvProdutos;
        private System.Windows.Forms.TextBox txtBuscarProduto;
        private System.Windows.Forms.Button btnBuscarProdutos;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Marca;
        private System.Windows.Forms.DataGridViewTextBoxColumn Categoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descricao;
    }
}
