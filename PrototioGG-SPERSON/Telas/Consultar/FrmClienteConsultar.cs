﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Cliente;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmClienteConsultar : UserControl
    {
        public FrmClienteConsultar()
        {
            InitializeComponent();
        }



        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            string Nome = txtBuscarCliente.Text;
            BusinessCliente bus = new BusinessCliente();
            List<DTOcliente> listar = bus.Lista(Nome);

            dgvClientes.AutoGenerateColumns = false;
            dgvClientes.DataSource = listar;



        }
    }
}
