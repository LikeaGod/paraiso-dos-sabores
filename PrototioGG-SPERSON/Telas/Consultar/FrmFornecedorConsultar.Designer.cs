﻿namespace PrototioGG_SPERSON.Telas.Consultar
{
    partial class FrmFornecedorConsultar
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelConsultarFORNECEDORES = new System.Windows.Forms.Panel();
            this.btnBuscarFornecedor = new System.Windows.Forms.Button();
            this.txtBuscarFornecedor = new System.Windows.Forms.TextBox();
            this.dgvFornecedor = new System.Windows.Forms.DataGridView();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNPJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CEP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bairro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rua = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label15 = new System.Windows.Forms.Label();
            this.panelConsultarFORNECEDORES.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFornecedor)).BeginInit();
            this.SuspendLayout();
            // 
            // panelConsultarFORNECEDORES
            // 
            this.panelConsultarFORNECEDORES.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelConsultarFORNECEDORES.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources.f5;
            this.panelConsultarFORNECEDORES.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelConsultarFORNECEDORES.Controls.Add(this.btnBuscarFornecedor);
            this.panelConsultarFORNECEDORES.Controls.Add(this.txtBuscarFornecedor);
            this.panelConsultarFORNECEDORES.Controls.Add(this.dgvFornecedor);
            this.panelConsultarFORNECEDORES.Controls.Add(this.label15);
            this.panelConsultarFORNECEDORES.Location = new System.Drawing.Point(0, 0);
            this.panelConsultarFORNECEDORES.Name = "panelConsultarFORNECEDORES";
            this.panelConsultarFORNECEDORES.Size = new System.Drawing.Size(984, 492);
            this.panelConsultarFORNECEDORES.TabIndex = 0;
            this.panelConsultarFORNECEDORES.Paint += new System.Windows.Forms.PaintEventHandler(this.panelConsultarFORNECEDORES_Paint);
            // 
            // btnBuscarFornecedor
            // 
            this.btnBuscarFornecedor.BackColor = System.Drawing.Color.White;
            this.btnBuscarFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarFornecedor.ForeColor = System.Drawing.Color.Black;
            this.btnBuscarFornecedor.Location = new System.Drawing.Point(192, 73);
            this.btnBuscarFornecedor.Name = "btnBuscarFornecedor";
            this.btnBuscarFornecedor.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarFornecedor.TabIndex = 3;
            this.btnBuscarFornecedor.Text = "Buscar";
            this.btnBuscarFornecedor.UseVisualStyleBackColor = false;
            this.btnBuscarFornecedor.Click += new System.EventHandler(this.btnBuscarFornecedor_Click);
            // 
            // txtBuscarFornecedor
            // 
            this.txtBuscarFornecedor.Location = new System.Drawing.Point(22, 75);
            this.txtBuscarFornecedor.Name = "txtBuscarFornecedor";
            this.txtBuscarFornecedor.Size = new System.Drawing.Size(164, 20);
            this.txtBuscarFornecedor.TabIndex = 2;
            // 
            // dgvFornecedor
            // 
            this.dgvFornecedor.AllowUserToAddRows = false;
            this.dgvFornecedor.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.dgvFornecedor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvFornecedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFornecedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.CNPJ,
            this.Email,
            this.Telefone,
            this.Estado,
            this.Cidade,
            this.CEP,
            this.Bairro,
            this.Rua});
            this.dgvFornecedor.GridColor = System.Drawing.SystemColors.HotTrack;
            this.dgvFornecedor.Location = new System.Drawing.Point(22, 137);
            this.dgvFornecedor.Name = "dgvFornecedor";
            this.dgvFornecedor.ReadOnly = true;
            this.dgvFornecedor.RowHeadersVisible = false;
            this.dgvFornecedor.Size = new System.Drawing.Size(939, 324);
            this.dgvFornecedor.TabIndex = 1;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "Fornecedor";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // CNPJ
            // 
            this.CNPJ.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CNPJ.DataPropertyName = "CNPJ";
            this.CNPJ.HeaderText = "CNPJ";
            this.CNPJ.Name = "CNPJ";
            this.CNPJ.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            // 
            // Telefone
            // 
            this.Telefone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Telefone.DataPropertyName = "Telefone";
            this.Telefone.HeaderText = "Telefone";
            this.Telefone.Name = "Telefone";
            this.Telefone.ReadOnly = true;
            // 
            // Estado
            // 
            this.Estado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Estado.DataPropertyName = "Estado";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            // 
            // Cidade
            // 
            this.Cidade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Cidade.DataPropertyName = "Cidade";
            this.Cidade.HeaderText = "Cidade";
            this.Cidade.Name = "Cidade";
            this.Cidade.ReadOnly = true;
            // 
            // CEP
            // 
            this.CEP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CEP.DataPropertyName = "CEP";
            this.CEP.HeaderText = "CEP";
            this.CEP.Name = "CEP";
            this.CEP.ReadOnly = true;
            // 
            // Bairro
            // 
            this.Bairro.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Bairro.DataPropertyName = "Bairro";
            this.Bairro.HeaderText = "Bairro";
            this.Bairro.Name = "Bairro";
            this.Bairro.ReadOnly = true;
            // 
            // Rua
            // 
            this.Rua.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Rua.DataPropertyName = "Rua";
            this.Rua.HeaderText = "Rua";
            this.Rua.Name = "Rua";
            this.Rua.ReadOnly = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Britannic Bold", 27.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(14, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(239, 41);
            this.label15.TabIndex = 0;
            this.label15.Text = "Fornecedores";
            // 
            // FrmFornecedorConsultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelConsultarFORNECEDORES);
            this.Name = "FrmFornecedorConsultar";
            this.Size = new System.Drawing.Size(984, 492);
            this.panelConsultarFORNECEDORES.ResumeLayout(false);
            this.panelConsultarFORNECEDORES.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFornecedor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelConsultarFORNECEDORES;
        private System.Windows.Forms.DataGridView dgvFornecedor;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnBuscarFornecedor;
        private System.Windows.Forms.TextBox txtBuscarFornecedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNPJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn CEP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bairro;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rua;
    }
}
