﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Departamento;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmDepartamentoConsultar : UserControl
    {
        public FrmDepartamentoConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarDepartamento_Click(object sender, EventArgs e)
        {
            string departamento = txtBuscarDepartamento.Text;
            BusinessDepartamento bus = new BusinessDepartamento();
            List<DTODepartamento> lista = bus.Listar(departamento);

            dgvDepartamento.AutoGenerateColumns = false;
            dgvDepartamento.DataSource = lista;

        }
    }
}
