﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Produto;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmProdutoConsultar : UserControl
    {
        public FrmProdutoConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarProdutos_Click(object sender, EventArgs e)
        {
            string nome = txtBuscarProduto.Text;

            BusinessProduto bus = new BusinessProduto();
            List<DTOProduto> listar = bus.Lista(nome);

            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = listar;
            
        }

        private void panelConsultarPRODUTOS_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtBuscarProduto_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
