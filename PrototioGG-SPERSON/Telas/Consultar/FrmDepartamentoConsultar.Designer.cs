﻿namespace PrototioGG_SPERSON.Telas.Consultar
{
    partial class FrmDepartamentoConsultar
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ddsadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dasdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelDepartamentoConsultar = new System.Windows.Forms.Panel();
            this.btnBuscarDepartamento = new System.Windows.Forms.Button();
            this.txtBuscarDepartamento = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvDepartamento = new System.Windows.Forms.DataGridView();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Salário = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1.SuspendLayout();
            this.panelDepartamentoConsultar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDepartamento)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddsadToolStripMenuItem,
            this.dadasToolStripMenuItem,
            this.dasdToolStripMenuItem,
            this.daToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(107, 92);
            // 
            // ddsadToolStripMenuItem
            // 
            this.ddsadToolStripMenuItem.Name = "ddsadToolStripMenuItem";
            this.ddsadToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.ddsadToolStripMenuItem.Text = "ddsad";
            // 
            // dadasToolStripMenuItem
            // 
            this.dadasToolStripMenuItem.Name = "dadasToolStripMenuItem";
            this.dadasToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.dadasToolStripMenuItem.Text = "dadas";
            // 
            // dasdToolStripMenuItem
            // 
            this.dasdToolStripMenuItem.Name = "dasdToolStripMenuItem";
            this.dasdToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.dasdToolStripMenuItem.Text = "dasd";
            // 
            // daToolStripMenuItem
            // 
            this.daToolStripMenuItem.Name = "daToolStripMenuItem";
            this.daToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.daToolStripMenuItem.Text = "da";
            // 
            // panelDepartamentoConsultar
            // 
            this.panelDepartamentoConsultar.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources.f3;
            this.panelDepartamentoConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelDepartamentoConsultar.Controls.Add(this.btnBuscarDepartamento);
            this.panelDepartamentoConsultar.Controls.Add(this.txtBuscarDepartamento);
            this.panelDepartamentoConsultar.Controls.Add(this.label1);
            this.panelDepartamentoConsultar.Controls.Add(this.dgvDepartamento);
            this.panelDepartamentoConsultar.Location = new System.Drawing.Point(0, 0);
            this.panelDepartamentoConsultar.Name = "panelDepartamentoConsultar";
            this.panelDepartamentoConsultar.Size = new System.Drawing.Size(984, 492);
            this.panelDepartamentoConsultar.TabIndex = 0;
            // 
            // btnBuscarDepartamento
            // 
            this.btnBuscarDepartamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarDepartamento.ForeColor = System.Drawing.Color.Black;
            this.btnBuscarDepartamento.Location = new System.Drawing.Point(253, 89);
            this.btnBuscarDepartamento.Name = "btnBuscarDepartamento";
            this.btnBuscarDepartamento.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarDepartamento.TabIndex = 3;
            this.btnBuscarDepartamento.Text = "Buscar";
            this.btnBuscarDepartamento.UseVisualStyleBackColor = true;
            this.btnBuscarDepartamento.Click += new System.EventHandler(this.btnBuscarDepartamento_Click);
            // 
            // txtBuscarDepartamento
            // 
            this.txtBuscarDepartamento.Location = new System.Drawing.Point(45, 89);
            this.txtBuscarDepartamento.Multiline = true;
            this.txtBuscarDepartamento.Name = "txtBuscarDepartamento";
            this.txtBuscarDepartamento.Size = new System.Drawing.Size(202, 23);
            this.txtBuscarDepartamento.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(37, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Departamento";
            // 
            // dgvDepartamento
            // 
            this.dgvDepartamento.AllowUserToAddRows = false;
            this.dgvDepartamento.AllowUserToDeleteRows = false;
            this.dgvDepartamento.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.dgvDepartamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDepartamento.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.Salário});
            this.dgvDepartamento.Location = new System.Drawing.Point(45, 155);
            this.dgvDepartamento.Name = "dgvDepartamento";
            this.dgvDepartamento.RowHeadersVisible = false;
            this.dgvDepartamento.Size = new System.Drawing.Size(690, 174);
            this.dgvDepartamento.TabIndex = 1;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "Nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            // 
            // Salário
            // 
            this.Salário.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Salário.DataPropertyName = "Salario";
            this.Salário.HeaderText = "Salário";
            this.Salário.Name = "Salário";
            // 
            // FrmDepartamentoConsultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelDepartamentoConsultar);
            this.Name = "FrmDepartamentoConsultar";
            this.Size = new System.Drawing.Size(984, 492);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panelDepartamentoConsultar.ResumeLayout(false);
            this.panelDepartamentoConsultar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDepartamento)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelDepartamentoConsultar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvDepartamento;
        private System.Windows.Forms.Button btnBuscarDepartamento;
        private System.Windows.Forms.TextBox txtBuscarDepartamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Salário;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ddsadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dasdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daToolStripMenuItem;
    }
}
