﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.Classes.Funcionario.ViewsFuncionario;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmFuncionariosConsultar : UserControl
    {
        public FrmFuncionariosConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarFuncionario_Click(object sender, EventArgs e)
        {
            DTOVwFuncionario dto = new DTOVwFuncionario();
            dto.Nome = txtBuscarFuncionario.Text.Trim();

            List<DTOVwFuncionario> list = new List<DTOVwFuncionario>();

            BusinessVwFuncionario bus = new BusinessVwFuncionario();
            list = bus.ConsultarView(dto);

            dgvFuncionarios.DataSource = list;
        }
    }
}
