﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Produto;
using PrototioGG_SPERSON.Plugin;
using PrototioGG_SPERSON.Telas.Consultar;

namespace PrototioGG_SPERSON.Telas.Alterar
{
    public partial class FrmProdutoAlterar : UserControl
    {
        private Label lblID;
        private Label label9;
        private Label label8;
        private PictureBox imgProduto;
        private NumericUpDown numericUpDown12;
        private Label label7;
        private NumericUpDown nudVlProduto;
        private Label label6;
        private Button button1;
        private TextBox txtDescricaoProduto;
        private TextBox txtCategoriaProduto;
        private TextBox txtMarcaProduto;
        private TextBox txtNomeProduto;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;

        public FrmProdutoAlterar()
        {
            InitializeComponent();
        }
        
        public void LoadScreen(DTOProduto dto)
        {
            lblID.Text = dto.ID.ToString();
            txtNomeProduto.Text = dto.Nome;
            txtMarcaProduto.Text = dto.Marca;
            txtDescricaoProduto.Text = dto.Descricao;
            txtCategoriaProduto.Text = dto.Categoria;
            nudVlProduto.Value = dto.Valor_Unit_Compra;
            numericUpDown12.Value = dto.Valor_Unit_Venda;
            imgProduto.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DTOProduto dto = new DTOProduto();
            dto.ID = Convert.ToInt32(lblID.Text);
            dto.Nome = txtNomeProduto.Text;
            dto.Marca = txtMarcaProduto.Text;
            dto.Descricao = txtDescricaoProduto.Text;
            dto.Categoria = txtCategoriaProduto.Text;
            dto.Valor_Unit_Compra = nudVlProduto.Value;
            dto.Valor_Unit_Venda = numericUpDown12.Value;
            dto.Imagem = ImagemPlugin.ConverterParaString(imgProduto.Image);

            BusinessProduto bus = new BusinessProduto();
            bus.Alterar(dto);

            MessageBox.Show("Alterado com Sucesso");

            panelProdutoAlterar.Controls.Clear();
            FrmProdutoConsultar frm = new FrmProdutoConsultar();
            panelProdutoAlterar.Controls.Add(frm);
        }

        private void imgProduto_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgProduto.ImageLocation = dialog.FileName;
            }
        }

        private void InitializeComponent()
        {
            this.lblID = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown12 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nudVlProduto = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDescricaoProduto = new System.Windows.Forms.TextBox();
            this.txtCategoriaProduto = new System.Windows.Forms.TextBox();
            this.txtMarcaProduto = new System.Windows.Forms.TextBox();
            this.txtNomeProduto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.imgProduto = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVlProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).BeginInit();
            this.SuspendLayout();
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.BackColor = System.Drawing.Color.Transparent;
            this.lblID.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.lblID.ForeColor = System.Drawing.Color.Gold;
            this.lblID.Location = new System.Drawing.Point(188, 188);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(0, 26);
            this.lblID.TabIndex = 55;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(138, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 26);
            this.label9.TabIndex = 54;
            this.label9.Text = "ID:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(766, 191);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 26);
            this.label8.TabIndex = 53;
            this.label8.Text = "Imagem";
            // 
            // numericUpDown12
            // 
            this.numericUpDown12.DecimalPlaces = 2;
            this.numericUpDown12.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.numericUpDown12.Location = new System.Drawing.Point(190, 314);
            this.numericUpDown12.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDown12.Name = "numericUpDown12";
            this.numericUpDown12.Size = new System.Drawing.Size(264, 26);
            this.numericUpDown12.TabIndex = 46;
            this.numericUpDown12.ThousandsSeparator = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(50, 308);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 26);
            this.label7.TabIndex = 51;
            this.label7.Text = "Valor Venda:";
            // 
            // nudVlProduto
            // 
            this.nudVlProduto.DecimalPlaces = 2;
            this.nudVlProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.nudVlProduto.Location = new System.Drawing.Point(190, 283);
            this.nudVlProduto.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nudVlProduto.Name = "nudVlProduto";
            this.nudVlProduto.Size = new System.Drawing.Size(264, 26);
            this.nudVlProduto.TabIndex = 44;
            this.nudVlProduto.ThousandsSeparator = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Book Antiqua", 27.75F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(9, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 45);
            this.label6.TabIndex = 50;
            this.label6.Text = "Produto";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkOrange;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(718, 500);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 37);
            this.button1.TabIndex = 49;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // txtDescricaoProduto
            // 
            this.txtDescricaoProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtDescricaoProduto.Location = new System.Drawing.Point(190, 419);
            this.txtDescricaoProduto.Multiline = true;
            this.txtDescricaoProduto.Name = "txtDescricaoProduto";
            this.txtDescricaoProduto.Size = new System.Drawing.Size(266, 119);
            this.txtDescricaoProduto.TabIndex = 48;
            // 
            // txtCategoriaProduto
            // 
            this.txtCategoriaProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtCategoriaProduto.Location = new System.Drawing.Point(191, 345);
            this.txtCategoriaProduto.Name = "txtCategoriaProduto";
            this.txtCategoriaProduto.Size = new System.Drawing.Size(264, 26);
            this.txtCategoriaProduto.TabIndex = 47;
            // 
            // txtMarcaProduto
            // 
            this.txtMarcaProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtMarcaProduto.Location = new System.Drawing.Point(190, 252);
            this.txtMarcaProduto.Name = "txtMarcaProduto";
            this.txtMarcaProduto.Size = new System.Drawing.Size(264, 26);
            this.txtMarcaProduto.TabIndex = 42;
            // 
            // txtNomeProduto
            // 
            this.txtNomeProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtNomeProduto.Location = new System.Drawing.Point(190, 221);
            this.txtNomeProduto.Name = "txtNomeProduto";
            this.txtNomeProduto.Size = new System.Drawing.Size(264, 26);
            this.txtNomeProduto.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(30, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 26);
            this.label5.TabIndex = 45;
            this.label5.Text = "Valor Compra:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(185, 387);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 26);
            this.label4.TabIndex = 43;
            this.label4.Text = "Descrição";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(108, 217);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 26);
            this.label3.TabIndex = 41;
            this.label3.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(104, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 26);
            this.label2.TabIndex = 40;
            this.label2.Text = "Marca:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(72, 340);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 26);
            this.label1.TabIndex = 38;
            this.label1.Text = "Categoria:";
            // 
            // imgProduto
            // 
            this.imgProduto.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.imgProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgProduto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgProduto.Location = new System.Drawing.Point(628, 221);
            this.imgProduto.Margin = new System.Windows.Forms.Padding(2);
            this.imgProduto.Name = "imgProduto";
            this.imgProduto.Size = new System.Drawing.Size(226, 244);
            this.imgProduto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgProduto.TabIndex = 52;
            this.imgProduto.TabStop = false;
            // 
            // FrmProdutoAlterar
            // 
            this.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources.k22;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.imgProduto);
            this.Controls.Add(this.numericUpDown12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.nudVlProduto);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtDescricaoProduto);
            this.Controls.Add(this.txtCategoriaProduto);
            this.Controls.Add(this.txtMarcaProduto);
            this.Controls.Add(this.txtNomeProduto);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmProdutoAlterar";
            this.Size = new System.Drawing.Size(957, 583);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVlProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
