﻿using PrototioGG_SPERSON.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrototioGG_SPERSON.Telas.Logar
{
    public partial class FrmUsuarioLogar : Form
    {
        public FrmUsuarioLogar()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            DTOFuncionario dto = new DTOFuncionario();
            dto.UserName = txtUsername.Text.Trim();
            dto.Password = txtSenha.Text.Trim();

            BusinessFuncionario bus = new BusinessFuncionario();
            dto = bus.Logou(dto);
            if(dto != null)
            {
                SessãoUsuario.UsuarioLogado = dto;
            }

           


        }


    }
}
