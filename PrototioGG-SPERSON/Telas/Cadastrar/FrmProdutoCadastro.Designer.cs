﻿namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    partial class FrmProdutoCadastro
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelProdutoCadastrar = new System.Windows.Forms.Panel();
            this.numericUpDown12 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nudVlProduto = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDescricaoProduto = new System.Windows.Forms.TextBox();
            this.txtCategoriaProduto = new System.Windows.Forms.TextBox();
            this.txtMarcaProduto = new System.Windows.Forms.TextBox();
            this.txtNomeProduto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelProdutoCadastrar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVlProduto)).BeginInit();
            this.SuspendLayout();
            // 
            // panelProdutoCadastrar
            // 
            this.panelProdutoCadastrar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelProdutoCadastrar.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources.k7;
            this.panelProdutoCadastrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelProdutoCadastrar.Controls.Add(this.numericUpDown12);
            this.panelProdutoCadastrar.Controls.Add(this.label7);
            this.panelProdutoCadastrar.Controls.Add(this.nudVlProduto);
            this.panelProdutoCadastrar.Controls.Add(this.label6);
            this.panelProdutoCadastrar.Controls.Add(this.button1);
            this.panelProdutoCadastrar.Controls.Add(this.txtDescricaoProduto);
            this.panelProdutoCadastrar.Controls.Add(this.txtCategoriaProduto);
            this.panelProdutoCadastrar.Controls.Add(this.txtMarcaProduto);
            this.panelProdutoCadastrar.Controls.Add(this.txtNomeProduto);
            this.panelProdutoCadastrar.Controls.Add(this.label5);
            this.panelProdutoCadastrar.Controls.Add(this.label4);
            this.panelProdutoCadastrar.Controls.Add(this.label3);
            this.panelProdutoCadastrar.Controls.Add(this.label2);
            this.panelProdutoCadastrar.Controls.Add(this.label1);
            this.panelProdutoCadastrar.Location = new System.Drawing.Point(0, 0);
            this.panelProdutoCadastrar.Name = "panelProdutoCadastrar";
            this.panelProdutoCadastrar.Size = new System.Drawing.Size(984, 492);
            this.panelProdutoCadastrar.TabIndex = 0;
            // 
            // numericUpDown12
            // 
            this.numericUpDown12.DecimalPlaces = 2;
            this.numericUpDown12.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.numericUpDown12.Location = new System.Drawing.Point(190, 230);
            this.numericUpDown12.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDown12.Name = "numericUpDown12";
            this.numericUpDown12.Size = new System.Drawing.Size(264, 26);
            this.numericUpDown12.TabIndex = 14;
            this.numericUpDown12.ThousandsSeparator = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(50, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 26);
            this.label7.TabIndex = 13;
            this.label7.Text = "Valor Venda:";
            // 
            // nudVlProduto
            // 
            this.nudVlProduto.DecimalPlaces = 2;
            this.nudVlProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.nudVlProduto.Location = new System.Drawing.Point(190, 199);
            this.nudVlProduto.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nudVlProduto.Name = "nudVlProduto";
            this.nudVlProduto.Size = new System.Drawing.Size(264, 26);
            this.nudVlProduto.TabIndex = 12;
            this.nudVlProduto.ThousandsSeparator = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Britannic Bold", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(4, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 41);
            this.label6.TabIndex = 11;
            this.label6.Text = "Produto";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.Lime;
            this.button1.Location = new System.Drawing.Point(762, 416);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 37);
            this.button1.TabIndex = 10;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtDescricaoProduto
            // 
            this.txtDescricaoProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtDescricaoProduto.Location = new System.Drawing.Point(510, 134);
            this.txtDescricaoProduto.Multiline = true;
            this.txtDescricaoProduto.Name = "txtDescricaoProduto";
            this.txtDescricaoProduto.Size = new System.Drawing.Size(387, 256);
            this.txtDescricaoProduto.TabIndex = 9;
            // 
            // txtCategoriaProduto
            // 
            this.txtCategoriaProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtCategoriaProduto.Location = new System.Drawing.Point(191, 261);
            this.txtCategoriaProduto.Name = "txtCategoriaProduto";
            this.txtCategoriaProduto.Size = new System.Drawing.Size(264, 26);
            this.txtCategoriaProduto.TabIndex = 8;
            // 
            // txtMarcaProduto
            // 
            this.txtMarcaProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtMarcaProduto.Location = new System.Drawing.Point(190, 168);
            this.txtMarcaProduto.Name = "txtMarcaProduto";
            this.txtMarcaProduto.Size = new System.Drawing.Size(264, 26);
            this.txtMarcaProduto.TabIndex = 6;
            // 
            // txtNomeProduto
            // 
            this.txtNomeProduto.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtNomeProduto.Location = new System.Drawing.Point(190, 137);
            this.txtNomeProduto.Name = "txtNomeProduto";
            this.txtNomeProduto.Size = new System.Drawing.Size(264, 26);
            this.txtNomeProduto.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(30, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 26);
            this.label5.TabIndex = 4;
            this.label5.Text = "Valor Compra:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(506, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 26);
            this.label4.TabIndex = 3;
            this.label4.Text = "Descrição:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(108, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(104, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Marca:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(72, 256);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Categoria:";
            // 
            // FrmProdutoCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelProdutoCadastrar);
            this.Name = "FrmProdutoCadastro";
            this.Size = new System.Drawing.Size(984, 492);
            this.panelProdutoCadastrar.ResumeLayout(false);
            this.panelProdutoCadastrar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVlProduto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelProdutoCadastrar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDescricaoProduto;
        private System.Windows.Forms.TextBox txtCategoriaProduto;
        private System.Windows.Forms.TextBox txtMarcaProduto;
        private System.Windows.Forms.TextBox txtNomeProduto;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudVlProduto;
        private System.Windows.Forms.NumericUpDown numericUpDown12;
        private System.Windows.Forms.Label label7;
    }
}
