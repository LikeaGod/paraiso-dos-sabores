﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Departamento;

namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    public partial class FrmDepartamentoCadastro : UserControl
    {
        public FrmDepartamentoCadastro()
        {
            InitializeComponent();
        }

        private void btnSalvarDepartamento_Click(object sender, EventArgs e)
        {
            DTODepartamento dto = new DTODepartamento();
            dto.Nome = txtNomeDepartamento.Text.Trim();
            dto.Salario = nudSalario.Value;
            BusinessDepartamento bus = new BusinessDepartamento();

            int id = bus.Salvar(dto);

            MessageBox.Show("salvo com sucesso");

        }
    }
}
