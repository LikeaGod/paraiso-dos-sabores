﻿namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    partial class FrmDepartamentoCadastro
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelDepartamentoCadastro = new System.Windows.Forms.Panel();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.btnSalvarDepartamento = new System.Windows.Forms.Button();
            this.txtNomeDepartamento = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelDepartamentoCadastro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            this.SuspendLayout();
            // 
            // panelDepartamentoCadastro
            // 
            this.panelDepartamentoCadastro.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources.k11;
            this.panelDepartamentoCadastro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelDepartamentoCadastro.Controls.Add(this.nudSalario);
            this.panelDepartamentoCadastro.Controls.Add(this.btnSalvarDepartamento);
            this.panelDepartamentoCadastro.Controls.Add(this.txtNomeDepartamento);
            this.panelDepartamentoCadastro.Controls.Add(this.label3);
            this.panelDepartamentoCadastro.Controls.Add(this.label2);
            this.panelDepartamentoCadastro.Controls.Add(this.label1);
            this.panelDepartamentoCadastro.Location = new System.Drawing.Point(0, 0);
            this.panelDepartamentoCadastro.Name = "panelDepartamentoCadastro";
            this.panelDepartamentoCadastro.Size = new System.Drawing.Size(984, 492);
            this.panelDepartamentoCadastro.TabIndex = 0;
            // 
            // nudSalario
            // 
            this.nudSalario.DecimalPlaces = 2;
            this.nudSalario.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.nudSalario.Location = new System.Drawing.Point(357, 211);
            this.nudSalario.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(230, 26);
            this.nudSalario.TabIndex = 6;
            this.nudSalario.ThousandsSeparator = true;
            // 
            // btnSalvarDepartamento
            // 
            this.btnSalvarDepartamento.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSalvarDepartamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvarDepartamento.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.btnSalvarDepartamento.ForeColor = System.Drawing.Color.Black;
            this.btnSalvarDepartamento.Location = new System.Drawing.Point(357, 257);
            this.btnSalvarDepartamento.Name = "btnSalvarDepartamento";
            this.btnSalvarDepartamento.Size = new System.Drawing.Size(230, 34);
            this.btnSalvarDepartamento.TabIndex = 5;
            this.btnSalvarDepartamento.Text = "Salvar";
            this.btnSalvarDepartamento.UseVisualStyleBackColor = false;
            this.btnSalvarDepartamento.Click += new System.EventHandler(this.btnSalvarDepartamento_Click);
            // 
            // txtNomeDepartamento
            // 
            this.txtNomeDepartamento.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtNomeDepartamento.Location = new System.Drawing.Point(357, 172);
            this.txtNomeDepartamento.Name = "txtNomeDepartamento";
            this.txtNomeDepartamento.Size = new System.Drawing.Size(230, 26);
            this.txtNomeDepartamento.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(272, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(257, 211);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Salário:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Departamento";
            // 
            // FrmDepartamentoCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelDepartamentoCadastro);
            this.Name = "FrmDepartamentoCadastro";
            this.Size = new System.Drawing.Size(984, 492);
            this.panelDepartamentoCadastro.ResumeLayout(false);
            this.panelDepartamentoCadastro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelDepartamentoCadastro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNomeDepartamento;
        private System.Windows.Forms.Button btnSalvarDepartamento;
        private System.Windows.Forms.NumericUpDown nudSalario;
    }
}
