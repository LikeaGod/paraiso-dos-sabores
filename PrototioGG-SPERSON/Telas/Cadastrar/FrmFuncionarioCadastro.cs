﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Funcionario;
using PrototioGG_SPERSON.DB.Departamento;

namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    public partial class FrmFuncionarioCadastro : UserControl
    {
        public FrmFuncionarioCadastro()
        {
            InitializeComponent();
            BusinessDepartamento bussss = new BusinessDepartamento();
            List<DTODepartamento> list = new List<DTODepartamento>();
            list = bussss.Listar2();
            List<string> liststring = new List<string>();
            foreach (DTODepartamento item in list)
            {
                liststring.Add(item.Nome);
            }
            comboBox1.DataSource = liststring;

        }

        private void btnSalvarFORNECEDOR_Click(object sender, EventArgs e)
        {
            DTOFuncionario dto = new DTOFuncionario();
            dto.Bairro = txtBairro.Text.Trim();
            dto.Telefone = txtTelefone.Text.Trim();
            dto.Celular = txtCelular.Text.Trim();
            dto.CEP = txtCEP.Text.Trim();
            dto.Rua = txtRua.Text.Trim();
            dto.Cidade = txtCidade.Text.Trim();
            dto.CPF = txtCPF.Text.Trim();
            dto.RG = txtRG.Text.Trim();
            dto.Email = txtEmail.Text.Trim();
            dto.UserName = txtUsername.Text.Trim();
            dto.Password = txtSenha.Text.Trim();
            dto.Estado = txtEstado.Text.Trim();
            dto.Funcionario = txtNome.Text.Trim();
            dto.Nascimento = dateTimePicker1.Value;
            DTODepartamento dtodepart = new DTODepartamento();
  
            dtodepart.Nome = comboBox1.SelectedItem.ToString();
            

            DTODepartamento dto2 = new DTODepartamento();
            dto2.Nome = comboBox1.SelectedItem.ToString();

            BusinessFuncionario busf = new BusinessFuncionario();
            int id = busf.Salvar(dto ,dto2);

            MessageBox.Show("Registrado com sucesso");
        }
    }
}
