﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Fornecedor;

namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    public partial class FrmFornecedorCadastro : UserControl
    {
        public FrmFornecedorCadastro()
        {
            InitializeComponent();
        }



        private void btnSalvarFORNECEDOR_Click(object sender, EventArgs e)
        {
            DTOFornecedor dto = new DTOFornecedor();
            dto.Fornecedor = txtNomeFornecedor.Text;
            dto.CNPJ = txtCNPJ.Text;
            dto.Bairro = txtBairroFornecedor.Text;
            dto.CEP = txtCEP.Text;
            dto.Cidade = txtCidadeFornecedor.Text;
            dto.Email = txtEmailFornecedor.Text;
            dto.Estado = txtEstadoFornecedor.Text;
            dto.Rua = txtRuaFornecedor.Text;
            dto.Telefone = txtTelefone.Text;

            BusinessFornecedor bus = new BusinessFornecedor();
            bus.Salvar(dto);

            MessageBox.Show("Salvo com Sucesso");
               
        }

        private void panelCadFORNECEDOR_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
