﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Cliente;

namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    public partial class FrmClienteCadastro : UserControl
    {
        public FrmClienteCadastro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DTOcliente dto = new DTOcliente();
            dto.Nome = txtNome.Text;
            dto.Bairro = txtBairro.Text;
            dto.Rua = txtRua.Text;
            dto.Estado = txtEstado.Text;
            dto.Cidade = txtCidade.Text;
            dto.CPF = txtCPF.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Celular = txtCelular.Text;
            dto.CEP = txtCEP.Text;
            dto.Email = txtEmail.Text;

            BusinessCliente bus = new BusinessCliente();
            bus.Salvar(dto);

            MessageBox.Show("salvo com sucesso");
        }

        private void panelClienteCadastro_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
