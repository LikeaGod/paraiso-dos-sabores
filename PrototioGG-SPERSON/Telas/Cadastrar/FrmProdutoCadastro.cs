﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Produto;

namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    public partial class FrmProdutoCadastro : UserControl
    {
        public FrmProdutoCadastro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DTOProduto dto = new DTOProduto();
            dto.Nome = txtNomeProduto.Text;
            dto.Marca = txtMarcaProduto.Text;
            dto.Descricao = txtDescricaoProduto.Text;
            dto.Categoria = txtCategoriaProduto.Text;
            dto.Valor_Unit_Compra = nudVlProduto.Value;
            dto.Valor_Unit_Venda = numericUpDown12.Value;

            BusinessProduto bus = new BusinessProduto();
            bus.Salvar(dto);

            MessageBox.Show("Salvo com Sucesso");

        }
    }
}
