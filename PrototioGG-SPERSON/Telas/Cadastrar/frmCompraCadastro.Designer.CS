﻿namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    partial class frmCompraCadastro
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label5 = new System.Windows.Forms.Label();
            this.imgProduto = new System.Windows.Forms.PictureBox();
            this.btnAddCarrinho = new System.Windows.Forms.Button();
            this.btnConfirmarCompraCadastro = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.nudQtdProduto = new System.Windows.Forms.NumericUpDown();
            this.cboFormPagamento = new System.Windows.Forms.ComboBox();
            this.cboFornecedor = new System.Windows.Forms.ComboBox();
            this.cboProdutos = new System.Windows.Forms.ComboBox();
            this.dgvCompraCadastro = new System.Windows.Forms.DataGridView();
            this.Produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Preço = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblTotalPreco = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtdProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompraCadastro)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(746, 481);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 26);
            this.label5.TabIndex = 34;
            this.label5.Text = "R$";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // imgProduto
            // 
            this.imgProduto.Location = new System.Drawing.Point(273, 146);
            this.imgProduto.Margin = new System.Windows.Forms.Padding(2);
            this.imgProduto.Name = "imgProduto";
            this.imgProduto.Size = new System.Drawing.Size(155, 158);
            this.imgProduto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgProduto.TabIndex = 33;
            this.imgProduto.TabStop = false;
            this.imgProduto.Click += new System.EventHandler(this.imgProduto_Click);
            // 
            // btnAddCarrinho
            // 
            this.btnAddCarrinho.BackColor = System.Drawing.Color.DarkGreen;
            this.btnAddCarrinho.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCarrinho.ForeColor = System.Drawing.Color.Black;
            this.btnAddCarrinho.Location = new System.Drawing.Point(377, 349);
            this.btnAddCarrinho.Name = "btnAddCarrinho";
            this.btnAddCarrinho.Size = new System.Drawing.Size(106, 31);
            this.btnAddCarrinho.TabIndex = 32;
            this.btnAddCarrinho.Text = "Adicionar";
            this.btnAddCarrinho.UseVisualStyleBackColor = false;
            this.btnAddCarrinho.Click += new System.EventHandler(this.btnAddCarrinho_Click);
            // 
            // btnConfirmarCompraCadastro
            // 
            this.btnConfirmarCompraCadastro.BackColor = System.Drawing.Color.DarkGreen;
            this.btnConfirmarCompraCadastro.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmarCompraCadastro.ForeColor = System.Drawing.Color.Black;
            this.btnConfirmarCompraCadastro.Location = new System.Drawing.Point(638, 511);
            this.btnConfirmarCompraCadastro.Name = "btnConfirmarCompraCadastro";
            this.btnConfirmarCompraCadastro.Size = new System.Drawing.Size(219, 33);
            this.btnConfirmarCompraCadastro.TabIndex = 31;
            this.btnConfirmarCompraCadastro.Text = "Confirmar";
            this.btnConfirmarCompraCadastro.UseVisualStyleBackColor = false;
            this.btnConfirmarCompraCadastro.Click += new System.EventHandler(this.btnConfirmarCompraCadastro_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(29, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 31);
            this.label4.TabIndex = 30;
            this.label4.Text = "Realizar Compra";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // nudQtdProduto
            // 
            this.nudQtdProduto.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudQtdProduto.Location = new System.Drawing.Point(272, 353);
            this.nudQtdProduto.Name = "nudQtdProduto";
            this.nudQtdProduto.Size = new System.Drawing.Size(99, 22);
            this.nudQtdProduto.TabIndex = 29;
            this.nudQtdProduto.ValueChanged += new System.EventHandler(this.nudQtdProduto_ValueChanged);
            // 
            // cboFormPagamento
            // 
            this.cboFormPagamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFormPagamento.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFormPagamento.FormattingEnabled = true;
            this.cboFormPagamento.Location = new System.Drawing.Point(273, 516);
            this.cboFormPagamento.Name = "cboFormPagamento";
            this.cboFormPagamento.Size = new System.Drawing.Size(211, 24);
            this.cboFormPagamento.TabIndex = 28;
            this.cboFormPagamento.SelectedIndexChanged += new System.EventHandler(this.cboFormPagamento_SelectedIndexChanged);
            // 
            // cboFornecedor
            // 
            this.cboFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFornecedor.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFornecedor.FormattingEnabled = true;
            this.cboFornecedor.Location = new System.Drawing.Point(273, 486);
            this.cboFornecedor.Name = "cboFornecedor";
            this.cboFornecedor.Size = new System.Drawing.Size(211, 24);
            this.cboFornecedor.TabIndex = 27;
            this.cboFornecedor.SelectedIndexChanged += new System.EventHandler(this.cboFornecedor_SelectedIndexChanged);
            // 
            // cboProdutos
            // 
            this.cboProdutos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProdutos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cboProdutos.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProdutos.FormattingEnabled = true;
            this.cboProdutos.Location = new System.Drawing.Point(272, 317);
            this.cboProdutos.Name = "cboProdutos";
            this.cboProdutos.Size = new System.Drawing.Size(211, 24);
            this.cboProdutos.TabIndex = 26;
            this.cboProdutos.SelectedIndexChanged += new System.EventHandler(this.cboProdutos_SelectedIndexChanged);
            // 
            // dgvCompraCadastro
            // 
            this.dgvCompraCadastro.BackgroundColor = System.Drawing.Color.DarkGreen;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCompraCadastro.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCompraCadastro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompraCadastro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Produto,
            this.Preço});
            this.dgvCompraCadastro.GridColor = System.Drawing.Color.Orange;
            this.dgvCompraCadastro.Location = new System.Drawing.Point(530, 132);
            this.dgvCompraCadastro.Name = "dgvCompraCadastro";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCompraCadastro.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCompraCadastro.RowHeadersVisible = false;
            this.dgvCompraCadastro.Size = new System.Drawing.Size(326, 172);
            this.dgvCompraCadastro.TabIndex = 25;
            this.dgvCompraCadastro.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCompraCadastro_CellContentClick);
            // 
            // Produto
            // 
            this.Produto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Produto.DataPropertyName = "Nome";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.Produto.DefaultCellStyle = dataGridViewCellStyle2;
            this.Produto.HeaderText = "Produto";
            this.Produto.Name = "Produto";
            // 
            // Preço
            // 
            this.Preço.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Preço.DataPropertyName = "Valor_Unit_Compra";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.Preço.DefaultCellStyle = dataGridViewCellStyle3;
            this.Preço.HeaderText = "Preço";
            this.Preço.Name = "Preço";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(164, 317);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 26);
            this.label6.TabIndex = 24;
            this.label6.Text = "Produto:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(129, 486);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 26);
            this.label7.TabIndex = 23;
            this.label7.Text = "Fornecedor:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // lblTotalPreco
            // 
            this.lblTotalPreco.AutoSize = true;
            this.lblTotalPreco.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalPreco.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.lblTotalPreco.ForeColor = System.Drawing.Color.Black;
            this.lblTotalPreco.Location = new System.Drawing.Point(784, 481);
            this.lblTotalPreco.Name = "lblTotalPreco";
            this.lblTotalPreco.Size = new System.Drawing.Size(66, 26);
            this.lblTotalPreco.TabIndex = 22;
            this.lblTotalPreco.Text = "00,00";
            this.lblTotalPreco.Click += new System.EventHandler(this.lblTotalPreco_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(128, 349);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 26);
            this.label3.TabIndex = 21;
            this.label3.Text = "Quantidade:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(633, 481);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 26);
            this.label2.TabIndex = 20;
            this.label2.Text = "Valor total:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(30, 511);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(239, 26);
            this.label1.TabIndex = 19;
            this.label1.Text = "Forma de Pagamento:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmCompraCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources.f22;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.imgProduto);
            this.Controls.Add(this.btnAddCarrinho);
            this.Controls.Add(this.btnConfirmarCompraCadastro);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nudQtdProduto);
            this.Controls.Add(this.cboFormPagamento);
            this.Controls.Add(this.cboFornecedor);
            this.Controls.Add(this.cboProdutos);
            this.Controls.Add(this.dgvCompraCadastro);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblTotalPreco);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmCompraCadastro";
            this.Size = new System.Drawing.Size(886, 617);
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtdProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompraCadastro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox imgProduto;
        private System.Windows.Forms.Button btnAddCarrinho;
        private System.Windows.Forms.Button btnConfirmarCompraCadastro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudQtdProduto;
        private System.Windows.Forms.ComboBox cboFormPagamento;
        private System.Windows.Forms.ComboBox cboFornecedor;
        private System.Windows.Forms.ComboBox cboProdutos;
        private System.Windows.Forms.DataGridView dgvCompraCadastro;
        private System.Windows.Forms.DataGridViewTextBoxColumn Produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Preço;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblTotalPreco;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}
