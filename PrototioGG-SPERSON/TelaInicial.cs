﻿using PrototioGG_SPERSON.Telas.Cadastrar;
using PrototioGG_SPERSON.Telas.Consultar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrototioGG_SPERSON
{
    public partial class TelaInicial : Form
    {
        public TelaInicial()
        {
            InitializeComponent();
        }

        private void funcionarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmFuncionarioCadastro tela = new FrmFuncionarioCadastro();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);

            }

            panelCentroinicial.Controls.Add(tela);

        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmClienteCadastro tela = new FrmClienteCadastro();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);

            }

            panelCentroinicial.Controls.Add(tela);

        }

        private void produtoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmProdutoCadastro tela = new FrmProdutoCadastro();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);
            }

            panelCentroinicial.Controls.Add(tela);
                

        }

        private void fornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmFornecedorCadastro tela = new FrmFornecedorCadastro();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);
            }

            panelCentroinicial.Controls.Add(tela);

        }

        private void funcionarioToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmFuncionariosConsultar tela = new FrmFuncionariosConsultar();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);
            }

            panelCentroinicial.Controls.Add(tela);


        }

        private void clienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmClienteConsultar tela = new FrmClienteConsultar();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);
            }

            panelCentroinicial.Controls.Add(tela);
        }

        private void fornecedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmFornecedorConsultar tela = new FrmFornecedorConsultar();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);
            }

            panelCentroinicial.Controls.Add(tela);
        }

        private void produtosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmProdutoConsultar tela = new FrmProdutoConsultar();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);
            }

            panelCentroinicial.Controls.Add(tela);

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void departamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmDepartamentoCadastro tela = new FrmDepartamentoCadastro();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);
            }

            panelCentroinicial.Controls.Add(tela);

        }

        private void departamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelCentroinicial.Controls.Clear();
            FrmDepartamentoConsultar tela = new FrmDepartamentoConsultar();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);
            }

            panelCentroinicial.Controls.Add(tela);


        }

        private void TelaInicial_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
    
}
